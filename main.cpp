#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
//
#include <vector>
//
#include "tree.h"
using namespace std;

int main()
{
	Tree<string> mytree;
    string output;
    int lineNumber=1;
    int userReq;
	ifstream myFile;
    myFile.open ("Ken.txt");
    vector<string> line;

    cout<< "\t\tWord\tLine" <<endl
        << "------------------------------------"<<endl;
    if(!myFile.eof())
    {
        while(getline(myFile,output))
        {
            line.push_back(output);
            istringstream split(output);
            while (split)
            {
                string word;
                split >> word;
                if(word == "") lineNumber++;
                else mytree.insert(word, lineNumber);
            }
        }
    }
    myFile.close();
    mytree.inorder();

    cout<<endl<<"Enter the line number:";
    cin>>userReq;
    if(userReq <= 0 || userReq > line.size()) cout<<"Not found"<<endl;
    else if(userReq == -1) return 0;
    else{ cout<<line.at(userReq-1)<<endl; }
    return 0;
}
